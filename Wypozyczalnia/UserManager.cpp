#include "UserManager.h"

UserManager* UserManager::instance = 0;

UserManager* UserManager::getInstance()
{
	if (instance == 0)
		instance = new UserManager();

	return instance;
}

UserManager::UserManager() {}

User UserManager::getUser()
{
	return UserManager::getInstance()->user;
}

void UserManager::Login(User user)
{
	UserManager::getInstance()->user = user;
}

bool UserManager::hasRole(std::string Role)
{
	return User::InRole(UserManager::getInstance()->getUser(), Role);
}