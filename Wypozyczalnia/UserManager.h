#pragma once
#include "User.h"

class UserManager
{

private:
	static UserManager* instance;
	User user;
	UserManager();

public:
	static UserManager* getInstance();
	static User getUser();
	static void Login(User user);
	static bool hasRole(std::string Role);
};

