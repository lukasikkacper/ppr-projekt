#include "Helpers.h"


std::string split(const std::string& str, const char ch, const unsigned int v)
{
	std::string ret = "";
	for (size_t i = 0, tmp = 0; i < str.size(); ++i) {
		if (str[i] == ch) {
			if (tmp > v) break;
			else ++tmp;

		}
		else if (tmp == v) ret += str[i];

	}
	return ret;
}

std::vector<std::string> splitToVector(std::string str, std::string token) {
	std::vector<std::string>result;
	while (str.size()) {
		int index = str.find(token);
		if (index != std::string::npos) {
			result.push_back(str.substr(0, index));
			str = str.substr(index + token.size());
			if (str.size() == 0)result.push_back(str);
		}
		else {
			result.push_back(str);
			str = "";
		}
	}
	return result;
}

std::string join(std::vector<std::string> arr, std::string delimiter)
{
	if (arr.empty()) return "";

	std::string str;
	for (auto i : arr)
		str += i + delimiter;
	str = str.substr(0, str.size() - delimiter.size());
	return str;
}