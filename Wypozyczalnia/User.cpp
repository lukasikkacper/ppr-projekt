#include "User.h"

const std::string FileName = "users.txt";

User::User(){}

User::User(std::string UserName, std::string UserPassword)
{
	this->UserName = UserName;
	this->UserPassword = UserPassword;
}

User::User(std::string UserName, std::string UserPassword, std::vector<std::string> Roles)
{
	this->UserName = UserName;
	this->UserPassword = UserPassword;
	this->UserRoles = Roles;
}

bool User::Register(std::string UserName, std::string Password)
{
	std::ifstream IdFile(FileName);
	std::ofstream File(FileName,std::ios::app);

	int Id = 1;

	if (IdFile.is_open())
	{
		std::string idLine;

		while (!IdFile.eof())
		{
			std::string tmp;
			std::getline(IdFile, tmp);

			if (tmp != "")
			{
				idLine = tmp;
				Id = std::stoi(split(idLine, ';', 0)) + 1;
			}
		}
		IdFile.close();
	}

	if (File.is_open())
	{
		File << Id << ";" << UserName << ";" << Password << ";" << "" << std::endl;
		File.close();
	}

	return true;
}

bool User::AddToRole(User User, std::string Role)
{
	std::ifstream File(FileName);
	std::ofstream newFile("Temp_" + FileName);
	std::string tmpName = "Temp_" + FileName;

	if (File.is_open())
	{
		std::string Line;

		while (std::getline(File, Line))
		{
			std::string _Id = split(Line, ';', 0);

			if (std::stoi(_Id) == User.Id)
			{
				std::string Id = split(Line, ';', 0);
				std::string Username = split(Line, ';', 1);
				std::string Password = split(Line, ';', 2);
				std::string Roles = split(Line, ';', 3);

				Roles += (Roles != "") ? "|" + Role : Role;

				Line = Id + ";" + Username + ";" + Password + ";" + Roles;
			}

			if (Line != "")
				newFile << Line << std::endl;
			else
				newFile << Line;
		}

		File.close();
		newFile.close();

		std::remove(FileName.c_str());
		rename(tmpName.c_str(), FileName.c_str());
	}

	return true;
}

bool User::AddToRoles(User User, std::vector<std::string> Role)
{
	std::ifstream File(FileName);
	std::ofstream newFile("Temp_" + FileName);
	std::string tmpName = "Temp_" + FileName;

	if (File.is_open())
	{
		std::string Line;

		while (std::getline(File, Line))
		{
			std::string _Id = split(Line, ';', 0);

			if (std::stoi(_Id) == User.Id)
			{
				std::string Id = split(Line, ';', 0);
				std::string Username = split(Line, ';', 1);
				std::string Password = split(Line, ';', 2);
				std::string Roles = split(Line, ';', 3);

				Roles += (Roles != "") ? "|" + join(Role,"|") : join(Role, "|");

				Line = Id + ";" + Username + ";" + Password + ";" + Roles;
			}

			if (Line != "")
				newFile << Line << std::endl;
			else
				newFile << Line;
		}

		File.close();
		newFile.close();

		std::remove(FileName.c_str());
		rename(tmpName.c_str(), FileName.c_str());
	}

	return true;
}

bool User::ChangePassword(int Id, std::string newPassword)
{
	return true;
}

User User::Login(std::string UserName, std::string Password)
{
	std::ifstream File(FileName);

	if (File.is_open())
	{
		std::string Line;

		while (std::getline(File, Line))
		{
			std::string Id = split(Line, ';', 0);
			std::string Username = split(Line, ';', 1);
			std::string Pass = split(Line, ';', 2);
			std::string Roles = split(Line, ';', 3);

			if (Username == UserName && Pass == Password)
			{
				User login = User();
				login.UserName = Username;
				login.UserRoles = splitToVector(Roles, "|");
				return login;
			}
			else
				throw "Blad Logowania";
		}

		File.close();
	}
}

bool User::InRole(User _User, std::string Role) 
{
	for (int i = 0; i < _User.UserRoles.size(); i++)
	{
		if (_User.UserRoles.at(i) == Role)
			return true;
	}
	
	return false;
}