#pragma once

#ifndef User_H
#define User_H

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "Helpers.h"

class User
{
public:
	int Id;
	std::string UserName;
	std::string UserPassword;
	std::vector<std::string> UserRoles;

	User();
	User(std::string UserName, std::string UserPassword);
	User(std::string UserName, std::string UserPassword,std::vector<std::string> Roles);

	static bool Register(std::string UserName, std::string Password);
	static bool AddToRole(User User, std::string Role);
	static bool AddToRoles(User User, std::vector<std::string> Role);
	static bool ChangePassword(int Id, std::string newPassword);
	static bool Delete(int Id);
	static User Login(std::string UserName, std::string Password);
	static bool InRole(User User, std::string Role);

};

#endif