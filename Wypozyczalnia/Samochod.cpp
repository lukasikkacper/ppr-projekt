#include "Samochod.h"


const std::string FileName = "cars.txt";


Samochod::Samochod(int Id,std::string Marka, std::string Model, int RokProdukcji)
{
	this->Id = Id;
	this->Marka = Marka;
	this->Model = Model;
	this->RokProdukcji = RokProdukcji;
}

Samochod::Samochod() {}

Samochod Samochod::getById(int Id)
{
	Samochod Temp;

	std::ifstream File(FileName);

	if (File.is_open())
	{
		std::string Line;

		while (std::getline(File, Line))
		{
			std::string _Id = split(Line, ';', 0);
			std::string _Marka = split(Line, ';', 1);
			std::string _Model = split(Line, ';', 2);
			std::string _RokProdukcji = split(Line, ';', 3);

			if (std::stoi(_Id) == Id)
			{
				Temp = Samochod(std::stoi(_Id), _Marka, _Model, std::stoi(_RokProdukcji));
				break;
			}	
		}

		File.close();
	}

	return Temp;
}

std::vector<Samochod> Samochod::getAll()
{
	std::vector<Samochod> Temp;

	std::ifstream File(FileName);

	if (File.is_open())
	{
		std::string Line;

		while (std::getline(File, Line))
		{
			std::string _Id = split(Line, ';', 0);
			std::string _Marka = split(Line, ';', 1);
			std::string _Model = split(Line, ';', 2);
			std::string _RokProdukcji = split(Line, ';', 3);

			Samochod toAdd = Samochod(std::stoi(_Id),_Marka, _Model, std::stoi(_RokProdukcji));

			Temp.push_back(toAdd);
		}

		File.close();
	}

	return Temp;
}

bool Samochod::deleteById(int Id)
{
	std::ifstream File(FileName);
	std::ofstream newFile("Temp_" + FileName);
	std::string tmpName = "Temp_" + FileName;

	if (File.is_open())
	{
		std::string Line;

		while (std::getline(File, Line))
		{
			std::string _Id = split(Line, ';', 0);

			if (std::stoi(_Id) == Id)
				Line = "";
			if(Line!="")
				newFile << Line << std::endl;
			else
				newFile << Line;
		}

		File.close();
		newFile.close();

		std::remove(FileName.c_str());
		rename(tmpName.c_str(), FileName.c_str());
	}

	return true;
}

bool Samochod::add(Samochod car)
{
	std::ifstream IdFile(FileName);
	std::ofstream File(FileName, std::ios::app);

	int Id = 1;

	if (IdFile.is_open())
	{
		std::string idLine;
		

		while (!IdFile.eof())
		{
			std::string tmp;
			std::getline(IdFile, tmp);

			if (tmp != "")
			{
				idLine = tmp;
				Id = std::stoi(split(idLine, ';', 0)) + 1;
			}
		}

		IdFile.close();
	}

	if (File.is_open())
	{
		File << Id << ";" << car.Marka << ";" << car.Model << ";" << car.RokProdukcji << std::endl;
		File.close();
	}

	return true;
}