#pragma once

#include <ostream>
#include <string>
#include <iterator>
#include <vector>

std::string split(const std::string& str, const char ch, const unsigned int v);

std::string join(std::vector<std::string> arr, std::string delimiter);

std::vector<std::string> splitToVector(std::string str, std::string token);