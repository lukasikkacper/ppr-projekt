#pragma once
#ifndef Samochod_H
#define Samochod_H

#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "Helpers.h"


class Samochod
{
public:
	int Id;
	std::string Marka;
	std::string Model;
	int RokProdukcji;

	Samochod(int Id,std::string Marka, std::string Model, int RokProdukcji);
	Samochod();

	static std::vector<Samochod> getAll(int ID);
	static Samochod getById(int ID);
	static std::vector<Samochod> getAll();
	static bool deleteById(int ID);
	static bool add(Samochod car);

};

#endif