#pragma once
#include <string>
#include <iostream>
#include "UserManager.h"
#include <conio.h>

static class MenuManager
{
public:

	static void MenuWyboru();

	static void MenuDodawaniaSamochodu();

	static void MenuUsuwaniaSamochodu();

	static void MenuDodawaniaUzytkownika();

	static void MenuUsuwaniaUzytkownika();

	static void MenuWypozyczeniaSamochodu();

	static void MenuSpisWypozyczen();

	static void MenuLogowania();

};

