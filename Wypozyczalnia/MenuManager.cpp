#include "MenuManager.h"

void MenuManager::MenuWyboru()
{
	char x = 'A';

	while (x != '0')
	{
		
		std::cout << "=============================" << std::endl;
		std::cout << "=Wypozyczalnia aut osobowych=" << std::endl;
		std::cout << "=============================" << std::endl << std::endl;

		//TYLKO DLA ADMINA
		if (UserManager::getInstance()->hasRole("A"))
		{
			std::cout << "1) Dodaj samochod" << std::endl;
			std::cout << "2) Usun samochod" << std::endl;
			std::cout << "3) Dodaj uzytkownika" << std::endl;
			std::cout << "4) Usun uzytkownika" << std::endl;
			std::cout << "5) Spis wypozyczen" << std::endl;
			std::cout << "6) Spis aut" << std::endl;
			std::cout << "0) Wyjscie z programu" << std::endl;

			x = _getch();
			system("cls");

			switch (x)
			{
			case '1':
				MenuManager::MenuDodawaniaUzytkownika();
				break;
			case '2':
				MenuManager::MenuUsuwaniaSamochodu();
				break;
			case 3:
				MenuManager::MenuDodawaniaUzytkownika();
				break;
			case 4:
				MenuManager::MenuUsuwaniaUzytkownika();
				break;
			case 5:
				MenuManager::MenuSpisWypozyczen();
				break;
			}
		}

		if (UserManager::getInstance()->hasRole("P"))
		{

		}

		if (UserManager::getInstance()->hasRole("K"))
		{

		}

		


		//TYLKO PRACOWNIK I WYZEJ

		//TYLKO KLIENT
	}
}

void MenuManager::MenuDodawaniaSamochodu(){}

void MenuManager::MenuUsuwaniaSamochodu() {}

void MenuManager::MenuDodawaniaUzytkownika() {}

void MenuManager::MenuUsuwaniaUzytkownika() {}

void MenuManager::MenuWypozyczeniaSamochodu() {}

void MenuManager::MenuSpisWypozyczen() {}

void MenuManager::MenuLogowania()
{
	std::string userName, Password;
	while (true)
	{
		std::cout << "=Logowanie=" << std::endl;
		std::cout << "Login: ";
		std::cin >> userName;
		std::cout << std::endl << "Haslo: ";
		std::cin >> Password;
		std::cout << std::endl;

		try
		{
			UserManager::Login(User::Login(userName, Password));
			system("cls");
			break;
		}
		catch (...)
		{
			system("cls");
			std::cout << "Nie prawidlowe haslo lub login !" << std::endl;
		}
	}
}
